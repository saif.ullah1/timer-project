import React from "react";
import Display from "../ClockDisplay/ClockDisplay";
import Header from "../Header/Header";
import styles from "./Layout.module.scss"
export default class Layout extends React.Component{
   
      render() {
        return (
          <div className={styles.layout}>
              <header className={styles.header}>
                  <Header
              name="Tick Tick Clock"
              />
              </header>
              <main className={styles.clock}>
                  <Display/>
              </main>           
          </div>
        );
      }
}