import React,{Component} from "react";
type clockType={
    date:string,
    count:number
}
export default class Clock extends Component<{},clockType>{
    state:clockType={
        date:'',
        count:0
    }
    constructor(props:clockType){
        super(props);

        this.state={
            date : new Date().toLocaleTimeString(),
            count:0
        };
    }
    componentDidMount(){
        setInterval(()=>{
            this.tick()
        },3000);
    }

    componentDidUpdate(prevProps:clockType,prevState:clockType){
        if(this.state.date!==prevState.date){
            this.setState({
                count:this.state.count+1,
            })
        }
    }
    componentWillUnmount(){
        alert("Clock is getting delete")
    }
    tick=()=>{
        this.setState({
            date:new Date().toLocaleTimeString()
        });
    }
    render(){
        return(
            <div>
                <h3>{this.state.date}.</h3>
                <h5>update: {this.state.count}</h5>
            </div>
        )
    }
}