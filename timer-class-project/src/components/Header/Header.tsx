import React,{Component} from "react";
import styles from "./Header.module.scss"
export default class Header extends Component<{name: string}>{
    render=()=>(
        <div className={styles.header}>
            <h1>{this.props.name}</h1>
        </div>
    )
}