import styles from "./ClockDisplay.module.scss";
import React, { Component } from "react";
import Clock from "../Clock/Clock";
type displayType = {
  show: boolean;
};
export default class Display extends Component<{}, displayType> {
 
  constructor(props: displayType) {
    super(props);
    this.state = {
      show: true,
    };
  }
  deleteClock = () => {
    this.setState({
      show: !this.state.show    
    });
  };

  render() {
    return (
      <div className={styles.display}>
           
        <div>
          {this.state.show ? (
            <Clock />
          ) : (
            <h3>Clock is removed! please refres the page</h3>
          )}
        </div>
        
          <button onClick={this.deleteClock}>Delete</button>
        
      </div>
    );
  }
}
